#! /usr/bin/env python
# -*- coding: utf-8 -*-

"""Python Sudoku is a text and graphical program (gtk interface) to create or
resolve sudokus. It can also print a sudoku (1 or 4 sudokus in each page) and
write an image (png, jpeg, etc) with a sudoku.

For usage: pysdk-gui.py --help


Copyright (C) 2005-2008  Xosé Otero <xoseotero@users.sourceforge.net>

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA


Modification history:
2005/10/12  Antti Kuntsi
	Added options to set the region size.

"""

import sys
import os
import traceback
import optparse
# Python Sudoku internacionalization
import locale
import gettext


import codecs

# Wrap sys.stdout and sys.stderr is needed for optparse that use
# sys.stdout.write instead of print
# This was fixed in python version 2.5 and is not needed any more.
if sys.version_info[0] <= 2 and sys.version_info[1] <= 4:
    if sys.stdout.encoding:
        sys.stdout = codecs.getwriter(sys.stdout.encoding)(sys.stdout)
    if sys.stderr.encoding:
        sys.stderr = codecs.getwriter(sys.stderr.encoding)(sys.stderr)

# Wrap sys.stdout and sys.stderr is needed when is redirected
if not sys.stdout.encoding:
    sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
if not sys.stderr.encoding:
    sys.stderr = codecs.getwriter(locale.getpreferredencoding())(sys.stderr)


from pythonsudoku.config import localedir, options
locale.setlocale(locale.LC_ALL, '')
l10n = gettext.translation("pythonsudoku", localedir, fallback=True)
l10n.install(True)


from pythonsudoku.check_modules import has_reportlab, has_PIL, \
     has_pygtk
from pythonsudoku.gui import Gui
from pythonsudoku.info import Info


# send bugs here
BUG_URL = "http://sourceforge.net/tracker/?group_id=150356&atid=778307"


def whatis():
    """Print what is a sudoku."""
    print Info["whatis"]

def create_parser():
    """Create a optparse.OptionParser with all the Python Sudoku options."""
    usage = _(u"""
  %prog [GUI Options] [Print Options] [PDF Options] [Image Options] [INPUT.sdk]
  %prog --version | -h | -m | -w

  INPUT.sdk is a Python Sudoku file""")
    version = "%prog " + Info["version"]
    parser = optparse.OptionParser(usage=usage, version=version)
    parser.add_option("-m", "--modules",
                      action="store_true", dest="modules", default=False,
                      help=_(u"show the modules not found"))

    parser.add_option("-w", "--whatis",
                      action="store_true", dest="info", default=False,
                      help=_(u"information about what is sudoku"))

    group = optparse.OptionGroup(parser,
                                 _(u"Creation Options"))
    group.add_option("--difficulty",
                     action="store", type="string", dest="difficulty",
                     default=options.get("sudoku", "difficulty"),
                     help=_(u"set the difficulty of the sudoku (\"hard\", \"normal\", \"easy\") (\"%s\" is the default)") % options.get("sudoku", "difficulty"))
    group.add_option("--handicap",
                     action="store", type="int", dest="handicap",
                     default=options.getint("sudoku", "handicap"),
                     help=_(u"set the handicap of the sudoku (0 = insane, 1 = insane + 1 extra number, etc) (%d is the default)") % options.getint("sudoku", "handicap"))
    group.add_option("--region_width",
                     action="store", type="int", dest="region_width",
                     default=options.getint("sudoku", "region_width"),
                     help=_(u"set the region width. The board will be HxW grid of WxH grids (%d is the default)") % options.getint("sudoku", "region_width"))
    group.add_option("--region_height",
                     action="store", type="int", dest="region_height",
                     default=options.getint("sudoku", "region_height"),
                     help=_(u"set the region height. The board will be HxW grid of WxH grids (%d is the default)") % options.getint("sudoku", "region_height"))
    parser.add_option_group(group)

    if has_reportlab:
        group = optparse.OptionGroup(parser, _(u"Print Options"))
        print_command = options.get("print", "command")
        print_command_strings = [_(u"set the command to print (not value set)"),
                                   _(u"set the command to print (\"%s\" is the default)") % print_command]
        group.add_option("--print_command",
                         action="store", dest="print_command",
                         default=print_command,
                         help=print_command_strings[options.get("print", "command") != ""])
        parser.add_option_group(group)

        group = optparse.OptionGroup(parser,
                                     _(u"Print and PDF Options"))
        group.add_option("--page",
                         action="store", type="string", dest="page",
                         default=options.get("pdf", "page"),
                         help=_(u"set the page size (\"A4\", \"LEGAL\", \"LETTER\", etc) (\"%s\" is the default)") % options.get("pdf", "page"))
        group.add_option("--no_title",
                         action="store_true", dest="no_title",
                         default=False,
                         help=_(u"don't draw the title text"))
        group.add_option("--title_font",
                         action="store", type="string", dest="title_font",
                         default=options.get("pdf", "title_font"),
                         help=_(u"set the title font (\"%s\" is the default)") % options.get("pdf", "title_font"))
        group.add_option("--title_colour",
                         action="store", type="string", dest="title_colour",
                         default=options.get("pdf", "title_colour"),
                         help=_(u"set the title colour (\"black\", \"blue\", etc) (\"%s\" is the default)") % options.get("pdf", "title_colour"))
        group.add_option("--title_size",
                         action="store", type="int", dest="title_size",
                         default=options.getint("pdf", "title_size"),
                         help=_(u"set the title font size (%d is the default)") % options.getint("pdf", "title_size"))
        group.add_option("--no_filename",
                         action="store_true", dest="no_filename",
                         default=False,
                         help=_(u"don't draw the filename"))
        group.add_option("--filename_font",
                         action="store", type="string", dest="filename_font",
                         default=options.get("pdf", "filename_font"),
                         help=_(u"set the filename font (\"%s\" is the default)") % options.get("pdf", "filename_font"))
        group.add_option("--filename_colour",
                         action="store", type="string", dest="filename_colour",
                         default=options.get("pdf", "filename_colour"),
                         help=_(u"set the filename colour (\"black\", \"blue\", etc) (\"%s\" is the default)") % options.get("pdf", "filename_colour"))
        group.add_option("--filename_size",
                         action="store", type="int", dest="filename_size",
                         default=options.getint("pdf", "filename_size"),
                         help=_(u"set the filename size (%d is the default)") % options.getint("pdf", "filename_size"))
        group.add_option("--four",
                         action="store_true", dest="four", default=False,
                         help=_(u"show 4 sudokus instead of 1"))
        group.add_option("--show_fonts",
                         action="store_true", dest="show_fonts",
                         default=False,
                         help=_(u"show valids fonts"))
        parser.add_option_group(group)

    if has_PIL:
        group = optparse.OptionGroup(parser, _(u"Image Options"))
        group.add_option("--format",
                         action="store", type="string", dest="format",
                         default=options.get("image", "format"),
                         help=_(u"set the image format (\"png\", \"jpeg\", etc) (\"%s\" is the default)") % options.get("image", "format"))
        group.add_option("--width",
                         action="store", type="int", dest="width",
                         default=options.getint("image", "width"),
                         help=_(u"set the image width in pixels (%d is the default)") % options.getint("image", "width"))
        group.add_option("--height",
                         action="store", type="int", dest="height",
                         default=options.getint("image", "height"),
                         help=_(u"set the image height in pixels (%d is the default)") % options.getint("image", "height"))
        group.add_option("--no_background",
                         action="store_true", dest="no_background",
                         default=False,
                         help=_(u"create a transparent image without background (if the format doesn't support transparency a black background is created)"))
        group.add_option("--background",
                         action="store", type="string", dest="background",
                         default=options.get("image", "background"),
                         help=_(u"set the image background (\"white\", \"blue\", etc) (\"%s\" is the default)") % options.get("image", "background"))
        parser.add_option_group(group)

    if has_reportlab or has_PIL:
        group = optparse.OptionGroup(parser,
                                     _(u"Print, PDF and Image Options"))
        group.add_option("--lines",
                         action="store", type="string", dest="lines",
                         default=(options.get("image", "lines_colour"),
                                  options.get("pdf", "lines_colour")),
                         help=_(u"set the lines colour (\"black\", \"blue\", etc) (\"%s\" is the default for Image and \"%s\" for PDF/Print)") % (options.get("image", "lines_colour"), options.get("pdf", "lines_colour")))
        group.add_option("--font",
                         action="store", type="string", dest="font",
                         default=(options.get("image", "font"), options.get("pdf", "font")),
                         help=_(u"set the font for the numbers (absolute path or relative to the script) (\"%s\" is the default for Image and \"%s\" for PDF/Print)") % (options.get("image", "font"), options.get("pdf", "font")))
        group.add_option("--font_size",
                         action="store", type="int", dest="font_size",
                         default=(options.getint("image", "font_size"),
                                  options.getint("pdf", "font_size")),
                         help=_(u"set the font size for the numbers (%d is the default for image and %d for PDF/Print)") % (options.getint("image", "font_size"), options.getint("pdf", "font_size")))
        group.add_option("--font_colour",
                         action="store", type="string", dest="font_colour",
                         default=(options.get("image", "font_colour"),
                                  options.get("pdf", "font_colour")),
                         help=_(u"set the font colour for the numbers (\"black\", \"blue\", etc) (\"%s\" is the default for Image and \"%s\" for PDF/Print)") % (options.get("image", "font_colour"), options.get("pdf", "font_colour")))
        parser.add_option_group(group)

    group = optparse.OptionGroup(parser, _(u"Visualization Options"))
    if options.getboolean("sudoku", "use_letters"):
        help_use_letters = _(u"show letters instead of numbers > 9 (default)")
        help_numbers_only = _(u"show only numbers")
    else:
        help_use_letters = _(u"show letters instead of numbers > 9")
        help_numbers_only = _(u"show only numbers (default)")
    group.add_option("--use_letters",
                     action="store_true", dest="use_letters",
                     default=options.getboolean("sudoku", "use_letters"),
                     help=help_use_letters)
    group.add_option("--numbers_only",
                     action="store_false", dest="use_letters",
                     default=not options.getboolean("sudoku", "use_letters"),
                     help=help_numbers_only)
    parser.add_option_group(group)

    return parser

def check_args(parser, options, args):
    """Check if the number of arguments if correct.

    Arguments:
    parser -- the optparse.OptionParser
    options -- then options returned by optparse.OptionParser.parser_args()
    args -- the args returned by options returned by
            optparse.OptionParser.parser_args()

    """
    length = len(args)

    if length > 1:
        parser.error(_(u"incorrect number of arguments"))

def set_options(opts):
    global options

    # 3rd param of SafeConfigParser.set() must be a string
    options.set("sudoku", "difficulty", opts.difficulty)
    options.set("sudoku", "handicap", str(opts.handicap))
    options.set("sudoku", "region_height", str(opts.region_height))
    options.set("sudoku", "region_width", str(opts.region_width))
    options.set("sudoku", "use_letters", str(opts.use_letters))

    if has_reportlab:
        options.set("print", "command", opts.print_command)

        options.set("pdf", "page", opts.page)
        if isinstance(opts.lines, tuple):
            options.set("pdf", "lines_colour", opts.lines[1])
        else:
            options.set("pdf", "lines_colour", opts.lines)
        if isinstance(opts.font, tuple):
            options.set("pdf", "font", opts.font[1])
        else:
            options.set("pdf", "font", opts.font)
        if isinstance(opts.font_colour, tuple):
            options.set("pdf", "font_colour", opts.font_colour[1])
        else:
            options.set("pdf", "font_colour", opts.font_colour)
        if isinstance(opts.font_size, tuple):
            options.set("pdf", "font_size", str(opts.font_size[1]))
        else:
            options.set("pdf", "font_size", str(opts.font_size))
        options.set("pdf", "title_font", opts.title_font)
        options.set("pdf", "title_colour", opts.title_colour)
        if opts.no_title:
            options.set("pdf", "title_size", str(0))
        else:
            options.set("pdf", "title_size", str(opts.title_size))
        options.set("pdf", "filename_font", opts.filename_font)
        options.set("pdf", "filename_colour", opts.filename_colour)
        if opts.no_filename:
            options.set("pdf", "filename_size", str(0))
        else:
            options.set("pdf", "filename_size", str(opts.filename_size))

    if has_PIL:
        options.set("image", "format", opts.format)
        options.set("image", "width", str(opts.width))
        options.set("image", "height", str(opts.height))
        if opts.no_background:
            options.set("image", "background", "")
        else:
            options.set("image", "background", opts.background)
        if isinstance(opts.lines, tuple):
            options.set("image", "lines_colour", opts.lines[0])
        else:
            options.set("image", "lines_colour", opts.lines)
        if isinstance(opts.font, tuple):
            options.set("image", "font", opts.font[0])
        else:
            options.set("image", "font", opts.font)
        if isinstance(opts.font_colour, tuple):
            options.set("image", "font_colour", opts.font_colour[0])
        else:
            options.set("image", "font_colour", opts.font_colour)
        if isinstance(opts.font_size, tuple):
            options.set("image", "font_size", str(opts.font_size[0]))
        else:
            options.set("image", "font_size", str(opts.font_size))


def main():
    parser = create_parser()
    (opts, args) = parser.parse_args()
    check_args(parser, opts, args)
    set_options(opts)

    try:
        import psyco
        psyco.full()
        has_psyco = True
    except ImportError:
        has_psyco = False

    if opts.modules:
        if not has_psyco:
            print _(u"You have not psyco installed, if you can, install it to get better performance")
        if not has_reportlab:
            print _(u"You have not reportlab installed, necessary to save as PDF and printing")
        if not has_PIL:
            print _(u"You have not PIL installed, necessary to save as image")
        if not has_pygtk:
            print _(u"You have not pygtk installed, necessary to gui")
    elif opts.info:
        whatis()
    else:                               # open gui
        if len(args) == 1:
            Gui(args[0])
        else:
            Gui()

    sys.exit()

if __name__ == "__main__":
    try:
        main()
    except (SystemExit, KeyboardInterrupt):
        pass
    except:
        print >> sys.stderr, _(u"\t-- cut here --")
        traceback.print_exc()
        print >> sys.stderr, _(u"\t-- cut here --")
        print >> sys.stderr
        print >> sys.stderr, _(u"An error has happened, please go to %s and send a bug report with the last lines.") % (BUG_URL)
